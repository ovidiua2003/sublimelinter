# Instructions

- install php 7.1 from http://windows.php.net/download#php-7.1
    * download the zip for your OS (x64, x86)
    * extract it in a folder on your harddrive, preferably a root folder on C
- add the folder where you extracted PHP to the windows environment variables
    * press Search, and write **"environment"**, choose **Edit the system environment variables**
    * click the **environment variables** button on the lower right of the opened window
    * in the **System Variables** panel, double-click the **Path** line
    * click **New** and add the folder like `C:\php\`
    * press **OK** until all windows close
- install Composer from https://getcomposer.org/Composer-Setup.exe; simply follow the installations wizard (this installer will also add the path to Composer in the system environment variables (`C:\ProgramData\ComposerSetup\bin`)
- **if you don't have Microsoft Visual C++ 2015 installed you will receive an error in the above installer about a missing DLL file**
- open a Command Prompt (cmd) and create a folder (preferably directly on C root) and change to it
    * **Install phpcs**
        * run `composer require "squizlabs/php_codesniffer=*"`
        * phpcs is installed
    * **Install phpmd**
        * run `composer require "phpmd/phpmd"`
        * phpmd is installed
    * **Install php-cs-fixer**
        * run `composer require friendsofphp/php-cs-fixer`
        * php-cs-fixer is installed
- add the `C:\folder-where-you-installed-phpcs\vendor\bin\` to the PATH **system environment variables**
- open Sublime 3 and 
    * install **SublimeLinter** package
    * in Sublime go to Preferences -> Package Settings -> SublimeLinter -> Settings and copy everything from left panel to right panel (overwrite what's in the right one)
    * install **SublimeLinter-php** package to have your php linted
    * install **SublimeLinter-phpcs** package for code sniffer
    * install **SublimeLinter-phpmd** package for mess detector
    * install **SublimeLinter-contrib-php-cs-fixer** package
- close Sublime 3 and reopen
- **DONE**

## PS ##
I would advise turning all these 4 linters off (maybe except php) in the package settings (setting **disabled** to true), and just turning them on in your desired projects. How you ask?

- Once you save your project, a sublime.project file is created. Open it up for editing and you will have something like this:
```
{
	"folders":
	[
		{
			"path": "love"
		}
	]
}
```
- now you just have to copy the linters you want enabled, above the folders entry. Here's an example of a final sublime project file:
```
{
    "SublimeLinter": {
        "linters": {
            "php": {
                "@disable": false,
            },
            "phpcs": {
                "@disable": false,
            },
            "phpcsfixer": {
                "@disable": false,
            },
            "phpmd": {
                "@disable": false,
            }
        },
    },
	"folders":
	[
		{
			"path": "love"
		}
	]
}

```
